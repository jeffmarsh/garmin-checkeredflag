using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Application as App;

class CheckeredFlagView extends Ui.WatchFace {

	var topRuler = null;
	var bottomRuler = null;
	var sunIcon = null;
	var moonIcon = null;

    function initialize() {
        WatchFace.initialize();
    }

    // Load your resources here
    function onLayout(dc) {
        setLayout(Rez.Layouts.WatchFace(dc));
        topRuler = Ui.loadResource(Rez.Drawables.TopRuler);
        bottomRuler = Ui.loadResource(Rez.Drawables.BottomRuler);
        sunIcon = Ui.loadResource(Rez.Drawables.Sun);
        moonIcon = Ui.loadResource(Rez.Drawables.Moon);
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() {
    }

    // Update the view
    function onUpdate(dc) {
    	var bg = Application.getApp().getProperty("CFBackgroundColor");
    	dc.setColor(bg, bg);
    	dc.clear();
    	drawBackground(dc);
        drawTime(dc);
    	drawTopRuler(dc);
    	drawBottomRuler(dc);
    }
    
    function drawBackground(dc){
    	var color = Application.getApp().getProperty("CFBackgroundColor");
    	dc.setColor(color, Gfx.COLOR_TRANSPARENT);
    	dc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());
    }
    
    function drawTopRuler(dc){
    	var vCenter = dc.getWidth()/2;
    	var hCenter = dc.getHeight()/2;
    	var bottomOfBar = 75;
    	var maxBarMove = -650;
    	var tfg = Application.getApp().getProperty("CFTopColor");
    	var bg = Application.getApp().getProperty("CFBackgroundColor");
    	var hg = Application.getApp().getProperty("CFHighlightColor");
    	
    	dc.setColor(tfg, Gfx.COLOR_TRANSPARENT);
    	dc.fillRectangle(0, 0, dc.getWidth(), bottomOfBar);
    	
    	// calculate Steps
    	var stepCount = ActivityMonitor.getInfo().steps;
        var stepGoal = ActivityMonitor.getInfo().stepGoal;
        var stepPercent = Math.floor((stepCount * 1.0 / stepGoal * 1.0) * 100).toNumber();
        var barX = (stepPercent * 5) * -1;
        if (barX <= maxBarMove){
        	barX = maxBarMove;
        }
        
    	dc.drawBitmap(barX,-3, topRuler);
    	//dc.drawBitmap(-500,-3, topRuler);
    	dc.setColor(bg, Gfx.COLOR_TRANSPARENT);
    	dc.fillPolygon([[hCenter-15, bottomOfBar],[hCenter, bottomOfBar-20], [hCenter+15, bottomOfBar]]);
    	dc.setColor(hg, Gfx.COLOR_TRANSPARENT);
    	//dc.setPenWidth(3);
    	dc.drawLine(hCenter, bottomOfBar-20,hCenter+15, bottomOfBar);
    	dc.drawLine(hCenter, bottomOfBar-20,hCenter-15, bottomOfBar);
    }
    
    function drawBottomRuler(dc){
    	var vCenter = dc.getWidth()/2;
    	var hCenter = dc.getHeight()/2;
    	var topOfBar = vCenter+50;
    	var maxBarMove = -500;
    	var bfg = Application.getApp().getProperty("CFBottomColor");
    	var bg = Application.getApp().getProperty("CFBackgroundColor");
    	var hg = Application.getApp().getProperty("CFHighlightColor");
    	dc.setColor(bfg, Gfx.COLOR_TRANSPARENT);
    	dc.fillRectangle(0, topOfBar, dc.getWidth(), dc.getHeight());
    	// calculate battery
    	
    	var floorCount = ActivityMonitor.getInfo().floorsClimbed;
        var floorGoal = ActivityMonitor.getInfo().floorsClimbedGoal;
        var floorPercent = Math.floor((floorCount * 1.0 / floorGoal * 1.0) * 100).toNumber();
        var barX = (floorPercent * 5) * -1;
        if (barX <= maxBarMove){
        	barX = maxBarMove;
        }
    	dc.drawBitmap(barX,topOfBar,bottomRuler);
    	dc.setColor(bg, Gfx.COLOR_TRANSPARENT);
    	dc.fillPolygon([[hCenter-15, topOfBar],[hCenter, topOfBar+20], [hCenter+15, topOfBar]]);
    	dc.setColor(hg, Gfx.COLOR_TRANSPARENT);
    	//dc.setPenWidth(3);
    	dc.drawLine(hCenter, topOfBar+20,hCenter+15, topOfBar);
    	dc.drawLine(hCenter, topOfBar+20,hCenter-15, topOfBar);
    }
    
    function drawTime(dc){
    	dc.setPenWidth(3);
    	var topOfTime = (dc.getHeight()/2)-30;
        var clockTime = Sys.getClockTime();
        var sun = true;
        var sunMoonPos = topOfTime+60;
        var sunOrMoon = sunIcon;
        //var timeString = Lang.format("$1$:$2$", [clockTime.hour, clockTime.min.format("%02d")]);
        var hour = clockTime.hour;
        var pm = false;
		var minColor = Application.getApp().getProperty("CFMinuteColor");
		var hourColor = Application.getApp().getProperty("CFHourColor");
        if (hour > 12){
        	pm = true;
        }
        
		var sunMoonPosArray = getsunMoonPos(hour, sun, sunMoonPos);
		sunMoonPos = sunMoonPosArray[0];
		sun = sunMoonPosArray[1];
		 
        var min = clockTime.min;
        if (!Sys.getDeviceSettings().is24Hour){
        	hour = hour % 12;
        	if (hour == 0){
        		hour=12;
        	}
        		
        }
        var hourFrmt = hour.format("%02d");
        var minFrmt = min.format("%02d");
        dc.setColor(hourColor, Gfx.COLOR_TRANSPARENT);
        dc.drawText(40,topOfTime,Gfx.FONT_SYSTEM_NUMBER_THAI_HOT,hourFrmt.toString(),Gfx.TEXT_JUSTIFY_LEFT);
        dc.setColor(minColor, Gfx.COLOR_TRANSPARENT);
        dc.drawText(130,topOfTime,Gfx.FONT_SYSTEM_NUMBER_THAI_HOT,minFrmt.toString(),Gfx.TEXT_JUSTIFY_LEFT);
       	
       	if (!sun){
       		sunOrMoon = moonIcon;
       	}
       	dc.drawBitmap(0,sunMoonPos,sunOrMoon);
       	
       	drawBattery(dc, topOfTime);
    }
    
    function getsunMoonPos(hrt, sun, sunMoonPos){
    	// calculate sun location
        // sunrise = 10 (10) 6AM
       	// highNoon = 150 (70) 12PM
       	var clc = 0;
       	
       	switch ( hrt ) {
       		case 1:
       			sun = false;
		    	clc=55;
		    break;
		    case 2:
		    	sun = false;
		    	clc=35;
		    break;
		    case 3:
		    	sun = false;
		    	clc=20;
		    break;
		    case 4:
		    	sun = false;
		    	clc=15;
		    break;
		    case 5:
		    	sun = false;
		    	clc=5;
		    break;
		    case 6: //sunrise
		    	sun = true;
		    	clc=10;
		    break;
		    case 7:
		    	sun = true;
		    	clc=20;
		    break;
		    case 8: 
		    	sun = true;
		    	clc=30;
		    break;
		    case 9:
		    	sun = true;
		    	clc=40;
		    break;
		    case 10:
		    	sun = true;
		    	clc=50;
		    break;
		    case 11:
		    	sun = true;
		    	clc=60;
		    break;
		    case 12:
		    	sun = true;
		    	clc=70;
		    break;
		    case 13:
		    	sun = true;
		    	clc=70;
		    break;
		    case 14:
		    	sun = true;
		    	clc=60;
		    break;
		    case 15: 
		    	sun = true;
		    	clc=50;
		    break;
		    case 16:
		    	sun = true;
		    	clc=40;
		    break;
		    case 17:
		    	sun = true;
		    	clc=30;
		    break;
		    case 18:
		    	sun = true;
		    	clc=20;
		    break;
		    case 19:
		    	sun = true;
		    	clc=10;
		    break;
		    case 20:
		    	sun = false;
		    	clc=10;
		    break;
		    case 21:
		    	sun = false;
		    	clc=25;
		    break;
		    case 22:
		    	sun = false;
		    	clc=45;
		    break;
		    case 23:
		    	sun = false;
		    	clc=60;
		    break;
		    case 24:
		    	sun = false;
		    	clc=70;
		    break;
		    default:
    			clc = -100;
    		break;
		   
		}
		sunMoonPos = sunMoonPos - clc;
		return [sunMoonPos, sun];
    }
    
    function drawBattery(dc, topOfTime){
    	
    	var batteryPercent = Sys.getSystemStats().battery;
       	var batMeterX = dc.getWidth()-25;
       	var batTop = topOfTime-15; // 75
       	var batHeight = 95;
       	var level = Math.floor((batteryPercent/100) * batHeight).toNumber();
       	var levely = (batHeight - level)+batTop;
       	var batColor = Application.getApp().getProperty("CFHighlightColor");
       	if (level < 23){
        	batColor = 0xFF0000;
        }
       	
       	dc.setColor(batColor, Gfx.COLOR_TRANSPARENT);
       	dc.drawRectangle(batMeterX, batTop, 15, batHeight); // draw the containing bar
       	dc.fillRectangle(batMeterX+2, levely, 11, level-2); // draw the bar itself
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() {
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() {
    }

}
